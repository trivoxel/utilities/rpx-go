# RPX-GO

A Linux command line utility that launches individual Wii U games and generates desktop entries for your library. It also includes several functions for Wii U games library management and Steam Big Picture Mode support planned.


*Notice: This is still early access software and is not yet fully featured.*


List of planned features:
* GUI game selection mode
* Automatic `.desktop` launcher generator
* Automatic Steam BPM title generator


Check the `TODO` entries in the file to see what all features can be expected and to see what is currently working. Some features may change in the future to expand functionality or for performance and stability reasons. All TODOs are marked "low" through "high" depending on their priority.