#!/bin/bash
### Runs Nintendo(R) WiiU(TM) .rpx executable files from a local database
### (C) 2021 Caden Mitchell (TriVoxel) under the MIT license. I would appreciate it if you credited me in your fork.
### https://trivoxel.page.link/site

##### DISCLAIMER #####
### Nintendo(R), the Nintendo(R) logo, WiiU(TM), and the WiiU(TM) logo are trademarks of Nintendo(R).
### All rights to games mentioned in this software are properties of their respective owners.
### As this is free software, it comes with no sort of warranty service and there is no guarantee that
### development builds of this software will work as intended. Not recommended for commercial use.
### DO NOT PIRATE GAMES THAT YOU CAN LEGALLY PURCHASE DIGITALLY OR PHYSICALLY. It is recommended that you
### only run games copied from a genuine WiiU console or that said games have been paid for and are legally
### owned by you. This software will not discern between authentic copies of games and pirated ones.
##### END OF DISCLAIMER #####

# TODO: Create a function to find all .rpx files from library instead of using hardcoded values and generate flags (@TriVoxel) P=Low
# TODO: Create a function that adds selected games to Steam library (@TriVoxel) P=Low
# TODO: Create a terminal-based GUI for a more user-friendly experience (@TriVoxel) P=Low
# TODO: Create a guided config process that generates a valid config file (@TriVoxel) P=High

source ~/.config/rpxgo/paths.conf # Needs variables "cemu" and "lib" for Cemu.exe path and Wii U library

## Logic
cemuProcess="$(basename "$cemu")"
.unixConv () {
  echo "${@////\\}"
}
.rpxGo () {
  pidof "$cemuProcess" >/dev/null && killall "$cemuProcess" # Sometimes Cemu keeps the process running after crashing or closing
  wine start /unix "$cemu" -g "Z:$(.unixConv $lib$1)" -f
  exit 0
}
.lsRpx () {
  find "$lib" -name "*.rpx" # Generates a list of WiiU executables
}
.makeDesktops () {
  exit 0 # TODO: Automatically creates desktop icons to launch Cemu games (@TriVoxel) P=Low
}
.makeSteam () {
  exit 0 # TODO: Automatically creates Steam library shortcuts for Big Picture Mode users (@TriVoxel) P=Low
}

## Titles
cttt="[USA] Captain Toad Treasure Tracker/code/Kinopio.rpx" # Captain Toad: Treasure Tracker
dkctf="[USA] Donkey Kong Country Tropical Freeze/code/rs10_production.rpx" # Donkey Kong Country: Tropical Freeze
dl="[USA] Dr. Luigi/code/rgm012.rpx" # Doctor Luigi
ffmobw="[USA] Fatal Frame Maiden of Black Water/code/lens5_cafe.rpx" # Fatal Frame: Maiden of Black Water
hwar="[USA] Hyrule Warriors/code/ProjectZ.rpx" # Hyrule Warriors
lozbotw="[USA] The Legend of Zelda Breath of the Wild/code/U-King.rpx" # The Legend of Zelda: Breath of the Wild
lozwwhd="[USA] The Legend of Zelda The Wind Waker HD/code/cking.rpx" # The Legend of Zelda: The Wind Waker HD
mcwue="[USA] Minecraft Wii U Edition/code/Minecraft.Client.rpx" # Minecraft: Wii U Edition
mk8="[USA] Mario Kart 8/code/Turbo.rpx" # Mario Kart 8
nsmbu="[USA] New Super Mario Bros. U/code/red-pro2.rpx" # New Super Mario Bros. U
rer="[USA] Resident Evil Revelations/code/BioRevHD.rpx" # Resident Evil: Revilations
sm3dw="[USA] Super Mario 3D World/code/RedCarpet.rpx" # Super Mario 3D World
smm="[USA] Super Mario Maker/code/Block.rpx" # Super Mario Maker
splatoon="[USA] Splatoon/code/Gambit.rpx" # Splatoon
ssbwu="[USA] Super Smash Bros. for Wii U/code/cross_f.rpx" # Super Smash Bros. For Wii U
xcx="[USA] Xenoblade Chronicles X/code/spaceTravel.rpx" # Xenoblade Chronicles X
yww="[USA] Yoshi’s Woolly World/code/pj023.rpx" # Yoshi's Wooly World
zombiu="[USA] ZombiU/code/LyN-Standalone_f.rpx" # ZombiU

## Execution
case "$1" in
  --util)
    case "$2" in
      ls) .lsRpx ;;
      dbg|debug) echo \
"lib: $lib
cemu: $cemu
cemuProcess: $cemuProcess
cemu running?: $(pidof "$cemuProcess" >/dev/null && echo "yes" || echo "no")
Example path: Z:$(.unixConv $lib$ssbwu)"
    ;;
    esac
  ;;
  ## Game launcher
  --cttt|--toad) .rpxGo "$cttt" ;; # Captain Toad: Treasure Tracker
  --dkctf|--kong) .rpxGo "$dkctf" ;; # Donkey Kong Country: Tropical Freeze
  --doctorluigi|--dl) .rpxGo "$dl" ;; # Doctor Luigi
  --ffmobw|--fatalframe|--maiden) .rpxGo "$ffmobw" ;; # Fatal Frame: Maiden of Black Water
  --hyrulewarriors|--hwar) .rpxGo "$hwar" ;; # Hyrule Warriors
  --lozbotw|--botw) .rpxGo "$lozbotw" ;;  # The Legend of Zelda: Breath of The Wild
  --lozwwhd|--wwhd) .rpxGo "$lozwwhd" ;; # The Legend of Zelda: The Wind Waker HD
  --minecraft|--mine|--mc|--mcwue) .rpxGo "$mcwue" ;; # Minecraft: Wii U Edition
  --mariokart8|--mk8) .rpxGo "$mk8" ;; # Mario Kart 8
  --newsupermariobrosu|--nsmbu) .rpxGo "$nsmbu" ;; # New Super Mario Bros. U
  --rer|--rerev) .rpxGo "$rer" ;; # Resident Evil: Revilations
  --supermario3dworld|--sm3dw) .rpxGo "$sm3dw" ;; # Super Mario 3D World
  --supermariomaker|--mariomaker|--smm) .rpxGo "$smm" ;; # Super Mario Maker
  --splatoon|--inkling) .rpxGo "$splatoon" ;; # Splatoon
  --supersmashbroswiiu|--smash|--ssbwu) .rpxGo "$ssbwu" ;; # Super Smash Bros. For Wii U
  --xenobladechroniclesx|--xenoblade|--xcx) .rpxGo "$xcx" ;; # Xenoblade Chronicles X
  --yoshiswoolyworld|--woolyworld|--yoshi|--wooly|--yww) .rpxGo "$yww" ;; # Yoshi's Wooly World
  --zombiu) .rpxGo "$zombiu" ;; # ZombiU
  *)
    echo "No game selected. Use -h flag for help."
  ;;
esac
